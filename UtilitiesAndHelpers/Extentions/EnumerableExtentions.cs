﻿using System;
using System.Collections.Generic;

namespace GhostmonkLibs.UtilitiesAndHelpers.Extentions
{
    public static class EnumerableExtentions
    {
        public static void ForEach<T>( this IEnumerable<T> list, Action<T> action )
        {
            foreach( T item in list ) { action( item ); }
        }
    }
}