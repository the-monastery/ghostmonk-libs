﻿using System.Runtime.InteropServices;

namespace GhostmonkLibs.UtilitiesAndHelpers.SystemInterop
{
    public class UserActions
    {
        [DllImport( "user32.dll" )]
        public static extern bool ExitWindowsEx( uint uFlags, uint dwReason );

        [DllImport( "user32.dll", SetLastError = true )]
        public static extern bool LockWorkStation();
    }
}
