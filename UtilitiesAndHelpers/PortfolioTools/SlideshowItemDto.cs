﻿namespace GhostmonkLibs.UtilitiesAndHelpers.PortfolioTools
{
    public class SlideshowItemDto
    {
        public string Title { get; set; }

        public string Url { get; set; }

        public string Description { get; set; }
    }
}
