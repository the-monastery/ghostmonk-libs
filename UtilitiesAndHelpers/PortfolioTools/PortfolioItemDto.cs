﻿using System;
using System.Collections.Generic;

namespace GhostmonkLibs.UtilitiesAndHelpers.PortfolioTools
{
    public class PortfolioItemDto
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }

        public string Client { get; set; }

        public string CodeLink { get; set; }

        public string VideoUrl { get; set; }

        public string Thumb { get; set; }

        public string Role { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public IEnumerable<SlideshowItemDto> SlideshowItems { get; set; }

        public IEnumerable<string> Skills { get; set; } 
    }
}