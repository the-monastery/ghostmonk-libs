﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace GhostmonkLibs.UtilitiesAndHelpers.PortfolioTools
{
    public static class XmlPortfolioLoader
    {
        public static List<PortfolioItemDto> LoadPortfolio( string xmlFilePath )
        {
            XDocument doc = XDocument.Load( xmlFilePath );
            return ( from item in doc.Element( "items" ).Elements( "item" ) select CreatePortfolioDto( item ) ).ToList();
        }

        private static PortfolioItemDto CreatePortfolioDto( XElement item )
        {
            return new PortfolioItemDto
            {
                Title = item.Element( "project" ).Value,
                Description = item.Element( "description" ).Value,
                Url = item.Element( "link" ).Value,
                Client = item.Attribute( "client" ).Value,
                CodeLink = item.Element( "codeLink" ).Value,
                VideoUrl = item.Element( "videoUrl" ).Value,
                Thumb = item.Element( "thumb" ).Value,
                Role = item.Element( "role" ).Value,
                StartDate = DateTime.Parse( item.Element( "startDate" ).Value ),
                EndDate = DateTime.Parse( item.Element( "endDate" ).Value ),
                Skills = item.Element( "technology" ).Value.Split( ',' ),
                SlideshowItems = from i in item.Element( "images" ).Elements( "img" ) select CreateSlideShowitem( i )
            };
        }

        private static SlideshowItemDto CreateSlideShowitem( XElement image )
        {
            return new SlideshowItemDto
            {
                Title = image.Attribute( "title" ).Value,
                Description = image.Attribute( "description" ).Value,
                Url = image.Attribute( "path" ).Value,
            };
        }
    }
}