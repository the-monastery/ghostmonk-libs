﻿using System;

namespace GhostmonkLibs.UtilitiesAndHelpers.Events
{
    public class EventArgs<T> : EventArgs
    {
        private readonly T eventData;

        public EventArgs( T eventData )
        {
            this.eventData = eventData;
        }

        public T EventData
        {
            get { return eventData; }
        }
    }
}