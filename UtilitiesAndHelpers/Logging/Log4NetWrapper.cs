﻿using System;
using log4net;

namespace GhostmonkLibs.UtilitiesAndHelpers.Logging
{
    public class Log4NetWrapper : ILogger
    {
        public ILog Logger { get; set; }

        public void Log( string msg )
        {
            Logger.Info( msg );
        }

        public void Log( string msg, string category )
        {
            Logger.Info( string.Format( "Msg: {0}, Category: {1}", msg, category ) );
        }

        public void Log( string msg, object info )
        {
            Logger.Info( string.Format( "Msg: {0}, Info:{1}", msg, info ) );
        }

        public void Log( string msg, string category, object info )
        {
            Logger.Info( string.Format( "Msg: {0}, Category: {1}, Info: {2}", msg, category, info ) );
        }

        public void Log( LogLevel level, string msg )
        {
            CheckLogLevel( level, msg );
        }

        public void Log( LogLevel level, string msg, string category )
        {
            CheckLogLevel( level, string.Format( "Msg: {0}, Category: {1}", msg, category ) );
        }

        public void Log( LogLevel level, string msg, object info )
        {
            CheckLogLevel( level, string.Format( "Msg: {0}, Info:{1}", msg, info ) );
        }

        public void Log( LogLevel level, string msg, string category, object info )
        {
            CheckLogLevel( level, string.Format( "Msg: {0}, Category: {1}, Info:{2}", msg, category, info ) );
        }

        public void Info( string msg )
        {
            Logger.Info( msg );
        }

        public void Info( string msg, Exception ex )
        {
            Logger.Info( msg, ex );
        }

        public void Warning( string msg )
        {
            Logger.Warn( msg );
        }

        public void Warning( string msg, Exception ex )
        {
            Logger.Warn( msg, ex );
        }

        public void Error( string msg )
        {
            Logger.Error( msg );
        }

        public void Error( string msg, Exception ex )
        {
            Logger.Error( msg, ex );
        }

        private void CheckLogLevel( LogLevel level, string msg )
        {
            switch( level )
            {
                case LogLevel.Info:
                    Info( msg );
                    break;
                case LogLevel.Warning:
                    Warning( msg );
                    break;
                case LogLevel.Error:
                    Error( msg );
                    break;
            }
        }
    }
}
