﻿using System;

namespace GhostmonkLibs.UtilitiesAndHelpers.Logging
{
    /// <summary>
    /// Instantiate this at the start of the application and set an
    /// instance of ILogger before calling Log anywhere. This is a wrapper 
    /// meant to encapsulate logging funtionality for easy swapping.
    /// </summary>
    public static class StaticLogger 
    {
        public static ILogger Instance { set; get; }

        public static void Info( string msg )
        {
            Instance.Info( msg );
        }

        public static void Info( string msg, Exception ex )
        {
            Instance.Info( msg, ex );
        }

        public static void Warning( string msg )
        {
            Instance.Warning( msg );
        }

        public static void Warning( string msg, Exception ex )
        {
            Instance.Warning( msg, ex );
        }

        public static void Error( string msg )
        {
            Instance.Error( msg );
        }

        public static void Error( string msg, Exception ex )
        {
            Instance.Error( msg, ex );
        }
    }
}
