﻿using System;

namespace GhostmonkLibs.UtilitiesAndHelpers.Logging
{
    public interface ILogger
    {
        void Log( string msg );

        void Log( string msg, string category );

        void Log( string msg, object info );

        void Log( string msg, string category, object info );


        void Log( LogLevel level, string msg );

        void Log( LogLevel level, string msg, string category );

        void Log( LogLevel level, string msg, object info );

        void Log( LogLevel level, string msg, string category, object info );

        
        void Info( string msg );

        void Info( string msg, Exception ex );

        void Warning( string msg );

        void Warning( string msg, Exception ex );

        void Error( string msg );
        
        void Error( string msg, Exception ex );
    }

    public enum LogLevel
    {
        Info, Warning, Error
    }
}