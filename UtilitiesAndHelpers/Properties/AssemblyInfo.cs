﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle( "Ghostmonk Utilities And Helpers" )]
[assembly: AssemblyDescription( "A library with various utilities methods, extention methods and other programmatic helpers." )]
[assembly: AssemblyConfiguration( "" )]
[assembly: AssemblyCompany( "Ghostmonk" )]
[assembly: AssemblyProduct( "Ghostmonk Utilities And Helpers" )]
[assembly: AssemblyCopyright( "Copyright ©  2012" )]
[assembly: AssemblyTrademark( "" )]
[assembly: AssemblyCulture( "" )]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible( false )]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid( "79a52ba4-bd1c-44d9-b14a-eb8e35a8f4ad" )]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion( "0.1.1.*" )]
[assembly: AssemblyFileVersion( "0.1.1.*" )]
[assembly: AssemblyInformationalVersion( "0.1 Alpha" )]
