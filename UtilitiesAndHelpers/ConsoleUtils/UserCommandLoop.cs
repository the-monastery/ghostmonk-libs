﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GhostmonkLibs.UtilitiesAndHelpers.ConsoleUtils
{
    public class UserCommandLoop
    {
        private const string EXIT = "exit";
        private readonly Dictionary<string, Action> actionMap = new Dictionary<string, Action>();
        private readonly StringBuilder helpMsg = new StringBuilder( string.Empty );

        public UserCommandLoop( string header )
        {
            helpMsg.Append( header.Trim() ).AppendLine().AppendLine();
            MapAction( OutputExitMessage, "Exit the program.", EXIT );
            MapAction( Help, "Print out help message.", "help", "?", "h" );
        }

        public void MapAction( Action action, string description, params string[] cmds )
        {
            string options = string.Empty;
            foreach( string cmd in cmds )
            {
                actionMap.Add( cmd, action );
                options += " /" + cmd;
            }

            if( !string.IsNullOrEmpty( description ) )
                helpMsg.Append( options ).Append( " " ).Append( description ).AppendLine();
        }

        public void Start()
        {
            string cmd = string.Empty;
            while( cmd != EXIT )
            {
                ConsoleOutput.ReturnWriteLine( "Enter a command." );
                cmd = Console.ReadLine();
                if( !string.IsNullOrEmpty( cmd ) && actionMap.ContainsKey( cmd ) )
                {
                    actionMap[ cmd ].Invoke();
                    continue;
                }
                ConsoleOutput.ReturnWriteLine( "Unknown command." );
                Help();
            }
        }

        private void Help()
        {
            ConsoleOutput.ReturnWriteLine( helpMsg.ToString() );
        }

        private static void OutputExitMessage()
        {
            ConsoleOutput.ReturnWriteLine( "Stopping application." );
        }
    }
}
