﻿using System;
using DirectShowLib;

namespace GhostmonkLibs.DirectShowWrappers.InterfaceExtensions
{
    public static class GraphBuilderExtentions
    {
        public static IBaseFilter GetFilter( this IGraphBuilder graphBuilder, Guid filterCategory, int index )
        {
            DsDevice[] devices = DsDevice.GetDevicesOfCat( filterCategory );
            object source;
            Guid id = typeof( IBaseFilter ).GUID;
            devices[ index ].Mon.BindToObject( null, null, ref id, out source );
            return ( IBaseFilter )source;
        }

        public static void SetFilter( this IGraphBuilder graphBuilder, IBaseFilter filter, string name )
        {
            DsError.ThrowExceptionForHR( graphBuilder.AddFilter( filter, name ) );
        }
    }
}
