﻿using System;
using DirectShowLib;

namespace GhostmonkLibs.DirectShowWrappers.InterfaceExtensions
{
    public static class CaptureGraphBuilderExtentions
    {
        public static void DoRenderStream( this ICaptureGraphBuilder2 graphBuilder, DsGuid pinCategory, DsGuid mediaType, object source, IBaseFilter compressor, IBaseFilter renderer )
        {
            DsError.ThrowExceptionForHR( graphBuilder.RenderStream( pinCategory, mediaType, source, compressor, renderer ) );
        }

        public static void SetFilterGraph( this ICaptureGraphBuilder2 captureGraph, IGraphBuilder graphBuilder )
        {
            DsError.ThrowExceptionForHR( captureGraph.SetFiltergraph( graphBuilder ) );
        }

        public static void SetOutputFile( this ICaptureGraphBuilder2 captureGraph, Guid type, string fileName, out IBaseFilter baseFilter, out IFileSinkFilter sink  )
        {
            DsError.ThrowExceptionForHR( captureGraph.SetOutputFileName( type, fileName, out baseFilter, out sink ) );
        }
    }
}
