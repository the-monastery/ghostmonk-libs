﻿using DirectShowLib;

namespace GhostmonkLibs.DirectShowWrappers.InterfaceExtensions
{
    public static class VideoWindowExtensions
    {
        public static void PutAutoShow( this IVideoWindow videoWindow, OABool autoShow )
        {
            DsError.ThrowExceptionForHR( videoWindow.put_AutoShow( autoShow ) );
        }
    }
}
