﻿using System;
using System.Runtime.InteropServices;
using DirectShowLib;
using GhostmonkLibs.DirectShowWrappers.InterfaceExtensions;

namespace GhostmonkLibs.DirectShowWrappers.WebCam
{
    public class AviCapture
    {
        private IGraphBuilder graphBuilder;
        private IMediaControl mediaControl;

        public AviCapture()
        {
            FileName = "recorded-file.avi";

            VideoInputDeviceIndex = 0;
            VideoCompressorIndex = -1;

            AudioInputDeviceIndex = 0;
            AudioCompressorIndex = -1;
        }
        
        public string FileName { get; set; }

        public int VideoInputDeviceIndex { get; set; }

        public int VideoCompressorIndex { get; set; }

        public int AudioInputDeviceIndex { get; set; }

        public int AudioCompressorIndex { get; set; }

        public void StartRecording()
        {
            InitGraph();
            if( mediaControl == null ) return;
            mediaControl.Run();
        }

        public void StopRecording()
        {
            mediaControl.Stop();
            ReleaseComObj( mediaControl );
            ReleaseComObj( graphBuilder );
        }

        private void InitGraph()
        {
            graphBuilder = (IGraphBuilder)new FilterGraph();
            ICaptureGraphBuilder2 captureGraphBuilder = ( ICaptureGraphBuilder2 )new CaptureGraphBuilder2();
            mediaControl = (IMediaControl)graphBuilder;
            captureGraphBuilder.SetFilterGraph( graphBuilder );

            IBaseFilter audioFilter = SetAndReturnFilter( FilterCategory.AudioInputDevice, AudioInputDeviceIndex, "audio" );
            IBaseFilter audioCompressor = SetAndReturnFilter( FilterCategory.AudioCompressorCategory, AudioCompressorIndex, "audioCompressor" );
            IBaseFilter videoFilter = SetAndReturnFilter( FilterCategory.VideoInputDevice, VideoInputDeviceIndex, "video" );
            IBaseFilter videoCompressor = SetAndReturnFilter( FilterCategory.VideoCompressorCategory, VideoCompressorIndex,  "videoCompressor" );
            
            IBaseFilter mux;
            IFileSinkFilter sink;
            captureGraphBuilder.SetOutputFile( MediaSubType.Avi, FileName, out mux, out sink );

            captureGraphBuilder.DoRenderStream( PinCategory.Capture, MediaType.Video, videoFilter, videoCompressor, mux );
            captureGraphBuilder.DoRenderStream( PinCategory.Capture, MediaType.Audio, audioFilter, audioCompressor, mux );

            ReleaseComObj( mux );
            ReleaseComObj( sink );
            ReleaseComObj( captureGraphBuilder );
        }

        private IBaseFilter SetAndReturnFilter( Guid id, int index, string name )
        {
            IBaseFilter filter = index > -1 ? graphBuilder.GetFilter( id, index ) : null;
            if( filter != null )
                graphBuilder.SetFilter( filter, name );
            return filter;
        }

        private static void ReleaseComObj( object obj )
        {
            Marshal.ReleaseComObject( obj );
        }
    }
}
