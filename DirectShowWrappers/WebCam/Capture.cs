using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using DirectShowLib;

namespace GhostmonkLibs.DirectShowWrappers.WebCam
{
    public class Capture : ISampleGrabberCB, IDisposable
    {
        private IFilterGraph2 graphBuilder;
        private IAMVideoControl videoControl;
        private IPin pinStill;

        private readonly ManualResetEvent pictureReady;

        private bool wantOne;
        private IntPtr bitmapDataBuffer = IntPtr.Zero;

        [DllImport( "Kernel32.dll", EntryPoint = "RtlMoveMemory" )]
        private static extern void CopyMemory( IntPtr Destination, IntPtr Source, [MarshalAs( UnmanagedType.U4 )] int Length );

        public Capture( int iDeviceNum, int iWidth, int iHeight, short iBpp )
        {
            DsDevice[] capDevices = DsDevice.GetDevicesOfCat( FilterCategory.VideoInputDevice );

            if( iDeviceNum + 1 > capDevices.Length )
                throw new Exception( "No video capture devices found at that index!" );

            try
            {
                SetupGraph( capDevices[ iDeviceNum ], iWidth, iHeight, iBpp );
                pictureReady = new ManualResetEvent( false );
            }
            catch
            {
                Dispose();
                throw;
            }
        }

        public void Dispose()
        {
            CloseInterfaces();
            if( pictureReady != null )
                pictureReady.Close();
        }

        ~Capture()
        {
            Dispose();
        }

        /// <summary>
        /// Get the image from the Still pin.  The returned image can turned into a bitmap with
        /// Bitmap b = new Bitmap(cam.Width, cam.Height, cam.Stride, PixelFormat.Format24bppRgb, m_ip);
        /// If the image is upside down, you can fix it with
        /// b.RotateFlip(RotateFlipType.RotateNoneFlipY);
        /// </summary>
        /// <returns>Returned pointer to be freed by caller with Marshal.FreeCoTaskMem</returns>
        public IntPtr Click()
        {
            // get ready to wait for new image
            pictureReady.Reset();
            bitmapDataBuffer = Marshal.AllocCoTaskMem( Math.Abs( Stride ) * Height );

            try
            {
                wantOne = true;

                // If we are using a still pin, ask for a picture
                if( videoControl != null )
                {
                    // Tell the camera to send an image
                    int hr = videoControl.SetMode( pinStill, VideoControlFlags.Trigger );
                    DsError.ThrowExceptionForHR( hr );
                }

                // Start waiting
                if( !pictureReady.WaitOne( 9000, false ) )
                {
                    throw new Exception( "Timeout waiting to get picture" );
                }
            }
            catch
            {
                Marshal.FreeCoTaskMem( bitmapDataBuffer );
                bitmapDataBuffer = IntPtr.Zero;
                throw;
            }

            return bitmapDataBuffer;
        }

        public int Width { get; private set; }

        public int Height { get; private set; }

        public int Stride { get; private set; }

        /// <summary> build the capture graph for grabber. </summary>
        private void SetupGraph( DsDevice dev, int iWidth, int iHeight, short iBPP )
        {
            int hr;

            ISampleGrabber sampGrabber = null;
            IPin pCaptureOut = null;
            IPin pSampleIn = null;
            IPin pRenderIn = null;

            // Get the graphbuilder object
            graphBuilder = new FilterGraph() as IFilterGraph2;

            try
            {
                // add the video input device
                IBaseFilter capFilter = null;
                hr = graphBuilder.AddSourceFilterForMoniker( dev.Mon, null, dev.Name, out capFilter );
                DsError.ThrowExceptionForHR( hr );

                pinStill = DsFindPin.ByCategory( capFilter, PinCategory.Still, 0 ) 
                    ?? DsFindPin.ByCategory( capFilter, PinCategory.Preview, 0 );

                // Still haven't found one.  Need to put a splitter in so we have
                // one stream to capture the bitmap from, and one to display.  Ok, we
                // don't *have* to do it that way, but we are going to anyway.
                if( pinStill == null )
                {
                    IPin pRaw = null;
                    IPin pSmart = null;

                    // There is no still pin
                    videoControl = null;

                    // Add a splitter
                    var iSmartTee = ( IBaseFilter )new SmartTee();

                    try
                    {
                        hr = graphBuilder.AddFilter( iSmartTee, "SmartTee" );
                        DsError.ThrowExceptionForHR( hr );

                        // Find the find the capture pin from the video device and the
                        // input pin for the splitter, and connnect them
                        pRaw = DsFindPin.ByCategory( capFilter, PinCategory.Capture, 0 );
                        pSmart = DsFindPin.ByDirection( iSmartTee, PinDirection.Input, 0 );

                        hr = graphBuilder.Connect( pRaw, pSmart );
                        DsError.ThrowExceptionForHR( hr );

                        // Now set the capture and still pins (from the splitter)
                        pinStill = DsFindPin.ByName( iSmartTee, "Preview" );
                        pCaptureOut = DsFindPin.ByName( iSmartTee, "Capture" );

                        // If any of the default config items are set, perform the config
                        // on the actual video device (rather than the splitter)
                        if( iHeight + iWidth + iBPP > 0 )
                        {
                            SetConfigParms( pRaw, iWidth, iHeight, iBPP );
                        }
                    }
                    finally
                    {
                        if( pRaw != null )
                        {
                            Marshal.ReleaseComObject( pRaw );
                        }
                        if( pRaw != pSmart )
                        {
                            Marshal.ReleaseComObject( pSmart );
                        }
                        if( pRaw != iSmartTee )
                        {
                            Marshal.ReleaseComObject( iSmartTee );
                        }
                    }
                }
                else
                {
                    // Get a control pointer (used in Click())
                    videoControl = capFilter as IAMVideoControl;

                    pCaptureOut = DsFindPin.ByCategory( capFilter, PinCategory.Capture, 0 );

                    // If any of the default config items are set
                    if( iHeight + iWidth + iBPP > 0 )
                    {
                        SetConfigParms( pinStill, iWidth, iHeight, iBPP );
                    }
                }

                // Get the SampleGrabber interface
                sampGrabber = new SampleGrabber() as ISampleGrabber;

                // Configure the sample grabber
                var baseGrabFlt = sampGrabber as IBaseFilter;
                ConfigureSampleGrabber( sampGrabber );
                pSampleIn = DsFindPin.ByDirection( baseGrabFlt, PinDirection.Input, 0 );

                // Get the default video renderer
                var pRenderer = new VideoRendererDefault() as IBaseFilter;
                hr = graphBuilder.AddFilter( pRenderer, "Renderer" );
                DsError.ThrowExceptionForHR( hr );

                pRenderIn = DsFindPin.ByDirection( pRenderer, PinDirection.Input, 0 );

                // Add the sample grabber to the graph
                hr = graphBuilder.AddFilter( baseGrabFlt, "Ds.NET Grabber" );
                DsError.ThrowExceptionForHR( hr );

                if( videoControl == null )
                {
                    // Connect the Still pin to the sample grabber
                    hr = graphBuilder.Connect( pinStill, pSampleIn );
                    DsError.ThrowExceptionForHR( hr );

                    // Connect the capture pin to the renderer
                    hr = graphBuilder.Connect( pCaptureOut, pRenderIn );
                    DsError.ThrowExceptionForHR( hr );
                }
                else
                {
                    // Connect the capture pin to the renderer
                    hr = graphBuilder.Connect( pCaptureOut, pRenderIn );
                    DsError.ThrowExceptionForHR( hr );

                    // Connect the Still pin to the sample grabber
                    hr = graphBuilder.Connect( pinStill, pSampleIn );
                    DsError.ThrowExceptionForHR( hr );
                }

                // Learn the video properties
                SaveSizeInfo( sampGrabber );
                ConfigVideoWindow();

                // Start the graph
                var mediaCtrl = graphBuilder as IMediaControl;
                hr = mediaCtrl.Run();
                DsError.ThrowExceptionForHR( hr );
            }
            finally
            {
                if( sampGrabber != null )
                {
                    Marshal.ReleaseComObject( sampGrabber );
                    sampGrabber = null;
                }
                if( pCaptureOut != null )
                {
                    Marshal.ReleaseComObject( pCaptureOut );
                    pCaptureOut = null;
                }
                if( pRenderIn != null )
                {
                    Marshal.ReleaseComObject( pRenderIn );
                    pRenderIn = null;
                }
                if( pSampleIn != null )
                {
                    Marshal.ReleaseComObject( pSampleIn );
                    pSampleIn = null;
                }
            }
        }

        private void SaveSizeInfo( ISampleGrabber sampGrabber )
        {
            var media = new AMMediaType();
            int hr = sampGrabber.GetConnectedMediaType( media );
            DsError.ThrowExceptionForHR( hr );

            if( ( media.formatType != FormatType.VideoInfo ) || ( media.formatPtr == IntPtr.Zero ) )
            {
                throw new NotSupportedException( "Unknown Grabber Media Format" );
            }

            // Grab the size info
            var videoInfoHeader = ( VideoInfoHeader )Marshal.PtrToStructure( media.formatPtr, typeof( VideoInfoHeader ) );
            Width = videoInfoHeader.BmiHeader.Width;
            Height = videoInfoHeader.BmiHeader.Height;
            Stride = Width * ( videoInfoHeader.BmiHeader.BitCount / 8 );

            DsUtils.FreeAMMediaType( media );
            media = null;
        }

        // Set the video window within the control specified by hControl
        private void ConfigVideoWindow()
        {
            int hr;

            var ivw = graphBuilder as IVideoWindow;
            ivw.put_AutoShow( OABool.False );

            // Set the parent
            //hr = ivw.put_Owner( hControl.Handle );
            //DsError.ThrowExceptionForHR( hr );

            // Turn off captions, etc
            //hr = ivw.put_WindowStyle( WindowStyle.Child | WindowStyle.ClipChildren | WindowStyle.ClipSiblings );
            //DsError.ThrowExceptionForHR( hr );

            // Yes, make it visible
            //hr = ivw.put_Visible( OABool.True );
            //DsError.ThrowExceptionForHR( hr );

            // Move to upper left corner
            //Rectangle rc = hControl.ClientRectangle;
            //hr = ivw.SetWindowPosition( 0, 0, rc.Right, rc.Bottom );
            //DsError.ThrowExceptionForHR( hr );
        }

        private void ConfigureSampleGrabber( ISampleGrabber sampGrabber )
        {
            int hr;
            var media = new AMMediaType();

            // Set the media type to Video/RBG24
            media.majorType = MediaType.Video;
            media.subType = MediaSubType.RGB24;
            media.formatType = FormatType.VideoInfo;
            hr = sampGrabber.SetMediaType( media );
            DsError.ThrowExceptionForHR( hr );

            DsUtils.FreeAMMediaType( media );
            media = null;

            // Configure the samplegrabber
            hr = sampGrabber.SetCallback( this, 1 );
            DsError.ThrowExceptionForHR( hr );
        }

        // Set the Framerate, and video size
        private void SetConfigParms( IPin pStill, int iWidth, int iHeight, short iBPP )
        {
            int hr;
            AMMediaType media;
            VideoInfoHeader v;

            var videoStreamConfig = pStill as IAMStreamConfig;

            // Get the existing format block
            hr = videoStreamConfig.GetFormat( out media );
            DsError.ThrowExceptionForHR( hr );

            try
            {
                // copy out the videoinfoheader
                v = new VideoInfoHeader();
                Marshal.PtrToStructure( media.formatPtr, v );

                // if overriding the width, set the width
                if( iWidth > 0 )
                {
                    v.BmiHeader.Width = iWidth;
                }

                // if overriding the Height, set the Height
                if( iHeight > 0 )
                {
                    v.BmiHeader.Height = iHeight;
                }

                // if overriding the bits per pixel
                if( iBPP > 0 )
                {
                    v.BmiHeader.BitCount = iBPP;
                }

                // Copy the media structure back
                Marshal.StructureToPtr( v, media.formatPtr, false );

                // Set the new format
                hr = videoStreamConfig.SetFormat( media );
                DsError.ThrowExceptionForHR( hr );
            }
            finally
            {
                DsUtils.FreeAMMediaType( media );
                media = null;
            }
        }

        /// <summary> Shut down capture </summary>
        private void CloseInterfaces()
        {
            int hr;

            try
            {
                if( graphBuilder != null )
                {
                    var mediaCtrl = graphBuilder as IMediaControl;

                    // Stop the graph
                    hr = mediaCtrl.Stop();
                }
            }
            catch( Exception ex )
            {
                Debug.WriteLine( ex );
            }

            if( graphBuilder != null )
            {
                Marshal.ReleaseComObject( graphBuilder );
                graphBuilder = null;
            }

            if( videoControl != null )
            {
                Marshal.ReleaseComObject( videoControl );
                videoControl = null;
            }

            if( pinStill != null )
            {
                Marshal.ReleaseComObject( pinStill );
                pinStill = null;
            }
        }

        /// <summary> sample callback, NOT USED. </summary>
        int ISampleGrabberCB.SampleCB( double SampleTime, IMediaSample pSample )
        {
            Marshal.ReleaseComObject( pSample );
            return 0;
        }

        /// <summary> buffer callback, COULD BE FROM FOREIGN THREAD. </summary>
        int ISampleGrabberCB.BufferCB( double SampleTime, IntPtr pBuffer, int BufferLen )
        {
            // Note that we depend on only being called once per call to Click.  Otherwise
            // a second call can overwrite the previous image.
            Debug.Assert( BufferLen == Math.Abs( Stride ) * Height, "Incorrect buffer length" );

            if( wantOne )
            {
                wantOne = false;
                Debug.Assert( bitmapDataBuffer != IntPtr.Zero, "Unitialized buffer" );

                // Save the buffer
                CopyMemory( bitmapDataBuffer, pBuffer, BufferLen );

                // Picture is ready.
                pictureReady.Set();
            }

            return 0;
        }
    }
}