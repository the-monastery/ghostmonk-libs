﻿using System;
using System.Linq;
using System.Net;

namespace GhostmonkLibs.Network.Server
{
    public class HtmlServer : BaseServer<HttpListenerContext>
    {
        private readonly HttpListener httpListener;

        public HtmlServer()
        {
            httpListener = new HttpListener();
        }

        public void AddConnectionPrefix( string prefix )
        {
            httpListener.Prefixes.Add( prefix );
        }

        public void ClearConnectionPrefixes()
        {
            httpListener.Prefixes.Clear();
        }

        protected override void OnStart()
        {
            Logger.Log( "Listening on the following prefixes: " + Environment.NewLine + httpListener.Prefixes.Aggregate( string.Empty, ( current, prefix ) => current + prefix ) );
            httpListener.Start();
        }

        protected override void OnStop()
        {
            if( httpListener.IsListening )
                httpListener.Close();
        }

        protected override HttpListenerContext GetConnection()
        {
            return httpListener.GetContext();
        }

        protected override string GetConnectionName( HttpListenerContext client )
        {
            IPEndPoint endpoint = client.Request.RemoteEndPoint;
            return endpoint != null ? endpoint.ToString() : string.Empty;
        }

        protected override void CloseConnection( HttpListenerContext client )
        {
            client.Response.Close();
        }
    }
}