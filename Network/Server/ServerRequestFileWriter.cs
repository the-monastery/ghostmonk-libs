﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using GhostmonkLibs.UtilitiesAndHelpers.Events;
using GhostmonkLibs.UtilitiesAndHelpers.Logging;

namespace GhostmonkLibs.Network.Server
{
    /// <summary>
    /// This will start up a HttpServer and write all incomming requests to a specified file.
    /// Use the Host or a Port and RequestPath combination to capture incomming requests.
    /// This class wraps the functionality used by the HttpListener, so the same patterns apply.
    /// </summary>
    public class ServerRequestFileWriter
    {
        private HtmlServer server;

        public ServerRequestFileWriter()
        {
            Port = -1;
            ResultsFile = "results.txt";
            RequestPath = string.Empty;
        }

        public ILogger Logger { get; set; }

        /// <summary>
        /// The full uri to listen to for incomming requests. Do not set the Port if 
        /// </summary>
        public Uri Host { get; set; }

        /// <summary>
        /// If a Port is specified, the host will be ignored, and all incomming requests on 
        /// the specified port will be logged. If the Port is set to 8080, the server will listen to all 
        /// requests on the following prefix. "http://*:8080/"
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// This variable can be used in conjunction with the Port. If the Port is specified, 
        /// the request can be further filtered by adding a path. The result of "some/path" with a port set to 8080
        /// will be a server listening to the following prefix."http://*:8080/some/path/"
        /// </summary>
        public string RequestPath { get; set; }

        /// <summary>
        /// The file where all incomming requests will be written
        /// </summary>
        public string ResultsFile { get; set; }

        /// <summary>
        /// Starts the server if it is not already running. The Logger, Host ant the Port and RequestPath must be set before 
        /// calling this method.
        /// </summary>
        public void Start()
        {
            if( server == null ) MakeServer();

            if( server.IsRunning ) return;

            string prefix = Port == -1 ? AppendSlash( Host.ToString() ) : BuildPortUrl();

            server.AddConnectionPrefix( prefix );
            server.Start();
        }

        /// <summary>
        /// Stops the server.
        /// </summary>
        public void Stop()
        {
            server.Stop();
            server.ClearConnectionPrefixes();
        }

        private string BuildPortUrl()
        {
            string output = "http://*:" + Port + "/";

            if( !string.IsNullOrEmpty( RequestPath ) )
            {
                if( RequestPath.StartsWith( "/" ) )
                    RequestPath = RequestPath.Remove( 0, 1 );
                RequestPath = AppendSlash( RequestPath );
            }
            
            return output + AppendSlash( RequestPath );
        }

        private void MakeServer()
        {
            server = new HtmlServer { Logger = Logger };
            server.ClientConnected += OnClientConnected;
            server.ClientException += OnClientExeption;
        }

        private static string AppendSlash( string input )
        {
            return input.EndsWith( "/" ) ? input : input + "/";
        }

        private void WriteRequestToFile( HttpListenerRequest request )
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append( "********************************" + Environment.NewLine );
            stringBuilder.Append( DateTime.Now + Environment.NewLine );
            stringBuilder.Append( "********************************" + Environment.NewLine );
            stringBuilder.Append( "Url : " + request.Url + Environment.NewLine );
            stringBuilder.Append( "Method : " + request.HttpMethod + Environment.NewLine );
            stringBuilder.Append( "RawUrl: " + request.RawUrl + Environment.NewLine );

            stringBuilder.Append( "QueryString : " + Environment.NewLine );
            NameValueCollection queries = request.QueryString;
            foreach( var key in queries.AllKeys )
            {
                stringBuilder.Append( "\t" + key + " :" + queries[ key ] + Environment.NewLine );
            }

            stringBuilder.Append( "Headers : " + Environment.NewLine );
            NameValueCollection headers = request.Headers;
            foreach( var key in headers.AllKeys )
            {
                stringBuilder.Append( "\t" + key + " :" + headers[ key ] + Environment.NewLine );
            }

            stringBuilder.Append( "UrlReferrer : " + request.UrlReferrer + Environment.NewLine );
            stringBuilder.Append( "UserAgent : " + request.UserAgent + Environment.NewLine );
            stringBuilder.Append( "UserHostAddress : " + request.UserHostAddress + Environment.NewLine );
            stringBuilder.Append( "UserHostName : " + request.UserHostName + Environment.NewLine );

            string languages = string.Empty;
            if( request.UserLanguages != null )
                languages = request.UserLanguages.Aggregate( string.Empty, ( current, language ) => current + ( language + ", " ) );
            stringBuilder.Append( "UserLanguages : " + languages + Environment.NewLine );

            stringBuilder.Append( Environment.NewLine + Environment.NewLine );

            using( StreamWriter writer = new StreamWriter( ResultsFile, true ) )
            {
                writer.Write( stringBuilder );
            }
            Console.WriteLine( stringBuilder );
        }

        private void OnClientConnected( object sender, EventArgs<HttpListenerContext> e )
        {
            WriteRequestToFile( e.EventData.Request );
        }

        private void OnClientExeption( object sender, EventArgs<Exception> e )
        {
            Logger.Error( "An error occurred on the client.", e.EventData );
        }
    }
}