﻿using System;
using System.Threading;
using GhostmonkLibs.UtilitiesAndHelpers.Events;
using GhostmonkLibs.UtilitiesAndHelpers.Logging;

namespace GhostmonkLibs.Network.Server
{
    public abstract class BaseServer<T>
    {
        private readonly TimeSpan abortTimeout = TimeSpan.FromSeconds( 1 );

        private readonly Object syncRoot;

        private Thread listenerThread;

        public event EventHandler<EventArgs<Exception>> ClientException = delegate { };

        public event EventHandler<EventArgs<T>> ClientConnected = delegate { };

        protected BaseServer()
        {
            syncRoot = new Object();
            Name = GetType().Name;
        }

        public string Name { set; get; }

        public bool IsRunning { get; private set; }

        public virtual bool IsSingleClient { get; set; }

        public ILogger Logger { get; set; }

        public void Start()
        {
            lock( syncRoot )
            {
                if( IsRunning ) return;

                listenerThread = new Thread( WaitForConnection )
                {
                    IsBackground = true,
                    Name = Name ?? GetType().Name
                };

                Logger.Log( String.Format( "Starting Server {0} [{1}]", Name, listenerThread.ManagedThreadId ) );

                OnStart();

                IsRunning = true;
                listenerThread.Start();
            }
        }

        public void Stop()
        {
            lock( syncRoot )
            {
                if( !IsRunning ) return;
                IsRunning = false;

                Logger.Log( String.Format( "Stopping Server {0} [{1}]", Name, listenerThread.ManagedThreadId ) );

                OnStop();

                if( listenerThread == null ) return;
                if( listenerThread.Join( abortTimeout ) ) return;

                listenerThread.Abort();
                listenerThread.Join( abortTimeout );
            }
        }

        protected abstract void OnStart();

        protected abstract void OnStop();

        protected abstract T GetConnection();

        protected abstract string GetConnectionName( T client );

        protected abstract void CloseConnection( T client );

        private void WaitForConnection()
        {
            while( IsRunning )
            {
                try
                {
                    T client = GetConnection();

                    if( IsSingleClient )
                    {
                        ClientThreadHandler( client );
                    }
                    else
                    {
                        Thread clientThread = new Thread( ClientThreadHandler )
                        {
                            IsBackground = true,
                            Name = string.Format( "{0} {1}", Name, GetConnectionName( client ) )
                        };
                        clientThread.Start( client );
                    }
                }
                catch( Exception ex )
                {
                    Logger.Log( LogLevel.Error, "Error waiting for client connection", ex );
                }
            }
        }

        private void ClientThreadHandler( object data )
        {
            T client = ( T )data;
            try
            {
                ClientConnected( this, new EventArgs<T>( client ) );
            }
            catch( Exception ex )
            {
                ClientException( this, new EventArgs<Exception>( ex ) );
            }
            finally
            {
                CloseConnection( client );
            }
        }
    }
}
