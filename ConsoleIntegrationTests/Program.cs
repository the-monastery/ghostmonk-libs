﻿using System;
using GhostmonkLibs.Network.Server;
using GhostmonkLibs.UtilitiesAndHelpers.Logging;
using log4net;

namespace ConsoleIntegrationTests
{
    public class Program
    {
        public Program()
        {
            StaticLogger.Instance = new Log4NetWrapper { Logger = LogManager.GetLogger( GetType() ) };
            ServerRequestFileWriter serverFileWriter = new ServerRequestFileWriter
            {
                Logger = StaticLogger.Instance,
                Host = new Uri( "http://localhost:8080/" )
            };
            serverFileWriter.Start();
        }

        static void Main( string[] args )
        {
            new Program();
            Console.ReadLine();
        }
    }
}
