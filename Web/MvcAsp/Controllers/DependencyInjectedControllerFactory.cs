﻿using System;
using System.ServiceModel.Channels;
using System.Web.Mvc;
using Microsoft.Practices.Unity;

namespace GhostmonkLibs.Web.MvcAsp.Controllers
{
    public class DependencyInjectedControllerFactory : DefaultControllerFactory
    {
        private readonly UnityContainer container;

        public DependencyInjectedControllerFactory( UnityContainer container )
        {
            this.container = container;
        }

        protected IController GetControllerInstance( RequestContext requestContext, Type controllerType )
        {
            return container.Resolve( controllerType ) as IController;
        }
    }
}
